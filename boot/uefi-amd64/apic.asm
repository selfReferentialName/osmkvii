	;; apic.asm
	;;
	;; handles the apic (and pic and pit for initialization)

M_PRT equ 0x20
S_PRT equ 0xa0
I_PRT equ 0x40

section .bss
extern pit_count
extern apic_ps					; nominal clock speed in picoseconds between clock ticks
extern sleep_done

section .rodata

init0_msg:
	db "APIC initialized. Nominal interonset interval of clock is 0x", 0
init1_msg:
	db " picoseconds.", 0x0a, 0x0d, 0
ckpoint:db "Bepis", 0x0a, 0x0d

section .text
global apic_init
global apic_eoi
global sleep
global timeout
extern idt_write
extern print
extern printrax
extern printal
extern printax

timeout:mov rcx, [rbx + apic_ps]		; sleep for rax picoseconds
	xor rdx, rdx
	div rcx					; get the number of ticks to check for timeout
	test rdx, rdx
	jz tlb0
	inc rax					; round up
tlb0:	push rax
	mov rax, slpirq
	mov dx, 0x8e20
	call idt_write
	xor ax, ax
	mov [rbx + sleep_done], al
	pop rax
	mov rcx, 0x1020
	mov rdx, 0xfee00320
	mov [rdx], ecx
	mov rdx, 0xfee00380
	mov [rdx], eax
	ret

slpirq:	push rax
	mov al, 1
	mov [rbx + sleep_done], al
	pop rax
	jmp apic_eoi

sleep:	mov rcx, [rbx + apic_ps]		; sleep for rax picoseconds
	xor rdx, rdx
	div rcx					; get the number of ticks to sleep for
	test rdx, rdx
	jz slb0
	inc rax					; round up
slb0:	push rax
	mov rax, slpirq
	mov dx, 0x8f20
	call idt_write
	xor ax, ax
	mov [rbx + sleep_done], al
	pop rax
	mov rcx, 0x1020
	mov rdx, 0xfee00320
	mov [rdx], ecx
	mov rdx, 0xfee00380
	mov [rdx], eax
	xor rax, rax
slp0:	nop
	cmp [rbx + sleep_done], al
	je slp0
	ret

apic_eoi:
spurious:
	push rax
	push rdx
	xor eax, eax
	mov edx, 0xfee000b0
	mov [rdx], eax
	pop rdx
	pop rax
	iretq

irq0:	push rax
	push rdx
	push rcx
	inc word [rbx + pit_count]
	mov al, 0x20
	mov dx, M_PRT
	out dx, al
	pop rcx
	pop rdx
	pop rax
	iretq

done:	push rdx
	push rax
	push rcx
	push rdi
	push rsi
	mov dx, I_PRT + 3
	mov al, 0x30
	out dx, al
	sub dx, 3
	mov al, 1
	out dx, al
	xor al, al
	out dx, al				; disable further IRQ0 interrupts
	mov dx, M_PRT + 1
	mov al, 0xff
	out dx, al				; completely
	cli
	xor rax, rax
	xor rdx, rdx
	mov ax, [rbx + pit_count]
	mov rsi, 119318
	mul rsi
	mov rcx, rax
	xor rdx, rdx
	mov rax, 100000000000000000
	test rcx, rcx
;	jnz dlb0
	mov rax, 100
	jmp dlb1
dlb0:	div rcx
dlb1:	mov [rbx + apic_ps], rax
	mov al, 1
	mov [rbx + sleep_done], al
	pop rsi
	pop rdi
	pop rcx
	pop rax
	pop rdx
	sti
	jmp apic_eoi

apic_init:
	; first, set up the PIC so we can use the PIT
	mov al, 0x10
	mov dx, M_PRT
	out dx, al
	mov dx, S_PRT
	out dx, al				; init
	mov al, 0xf0
	mov dx, M_PRT + 1
	out dx, al
	mov al, 0xf8
	mov dx, S_PRT + 1
	out dx, al				; offset
	mov al, 0x04
	mov dx, M_PRT + 1
	out dx, al
	mov al, 0x02
	mov dx, S_PRT + 1
	out dx, al				; cascading info
	mov al, 0xff
	out dx, al
	mov al, 0x00
	mov dx, M_PRT + 1
	out dx, al				; mask all IRQs except the PIT channel 0
	; now, set up the interrupts
	mov rax, irq0
	mov dx, 0x8ef0
	call idt_write				; add the irq0 interrupt to the IDT
	mov rax, done
	mov dx, 0x8e20
	call idt_write				; add the done interrupt as int 32
	int 0xf0
	mov rax, spurious
	mov dx ,0x8eff
	call idt_write				; add the spurious interrupt
	; now initialize the APIC
	mov ecx, 0x1b
	mov eax, 0xfee00800
	xor edx, edx
	wrmsr
	mov eax, 0x000001ff
	mov edx, 0xfee000f0
	mov [rdx], eax
	; now to time it
	xor rax, rax
	mov [rbx + pit_count], ax
	mov edx, 0xfee003e0
	mov [edx], eax
	mov eax, 0x00001020
	mov edx, 0xfee00320
	mov [rdx], eax
	mov rax, 1000000
	mov edx, 0xfee00380
	mov [rdx], eax
	mov dx, I_PRT + 3
	mov al, 0x34
	out dx, al
	sub dx, 3
	mov al, 1
	out dx, al
	xor al, al
	out dx, al
	xor rax, rax
	mov [rbx + apic_ps], rax
	mov [rbx + sleep_done], al
ilp0:	pause
	mov edx, 0xfee00390
	mov eax, [edx]
	test rax, rax
	jnz ilp0
	mov dx, I_PRT + 3
	mov al, 0x30
	out dx, al
	sub dx, 3
	mov al, 1
	out dx, al
	xor al, al
	out dx, al				; disable further IRQ0 interrupts
	mov dx, M_PRT + 1
	mov al, 0xff
	out dx, al				; completely
	; and we're done
	mov rax, init0_msg
	call print
	mov rax, [rbx + apic_ps]
	call printrax
	mov rax, init1_msg
	call print
	mov rax, 1
	call timeout
	ret
