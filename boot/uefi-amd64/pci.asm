	;; pci.asm
	;;
	;; handles pci setup and discovery
	;; can only register pci devices serially
	;; does not load pci drivers

extern config

section .rodata

ckpoint:db " is the current value of dx. The kitty is sleepy.", 0x0a, 0x0d, 0

section .text

global pci_readcfg			; read config -- uint32_t pci_readcfg(uint32_t address, uint8_t offset)
global pci_writecfg			; write config -- void pci_writecfg(uint32_t address, uint8_t offset, uint32_t data)
global pci_init
extern ahci_register
extern virtio_block_register
extern print
extern printax

ADDR_PRT equ 0xcf8
DATA_PRT equ 0xcfc

checkpoint:
	push rax
	push rcx
	push rdx
	push r8
	push r11
	mov ax, dx
	call printax
	mov rax, ckpoint
	call print
	pop r11
	pop r8
	pop rdx
	pop rcx
	pop rax
	ret

pci_readcfg:
	and edx, 0xff
	add eax, edx
	mov dx, 0xcf8
	out dx, eax
	mov dx, 0xcfc
	in eax, dx
	ret

pci_writecfg:
	and edx, 0xff
	add eax, edx
	mov dx, 0xcf8
	out dx, eax
	mov eax, edi
	mov dx, 0xcfc
	out dx, eax
	ret

register:				; eax = edi = config offset of the function
	cmp si, 0x1af4
	je rvirtio
	mov dx, ADDR_PRT
	add eax, 0x08
	out dx, eax
	mov dx, DATA_PRT
	in eax, dx
	mov cx, ax
	mov dx, cx
	shr eax, 16
	mov dx, ax
	mov eax, edi
	cmp dx, 0x0601			; SATA controller
	je rsata
	ret				; couldn't find it, ignore it

rsata:	cmp ch, 0x01
	je ahci_register		; register an AHCI controller
	ret

rvirtio:shr rsi, 16
	cmp si, 0x1001
	je virtio_block_register
	cmp si, 0x1042
	je virtio_block_register
	ret

pci_init:
	mov edi, 0x80000000
	mov rcx, 0x10000
ilp0:	mov rax, rdi
	mov dx, ADDR_PRT
	out dx, eax
	mov dx, DATA_PRT
	in eax, dx
	cmp ax, 0xffff
	je ilp0c0
	push rdi
	push rcx
	mov rsi, rax
	mov rax, rdi
	call register
	pop rcx
	pop rdi
ilp0c0:	add rdi, 0x100
	loop ilp0
	ret
