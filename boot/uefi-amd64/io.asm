	;; io.asm
	;;
	;; provides call wrappers for c io functions

section .text
global inb
global inw
global ind
global outb
global outw
global outd

inb:	mov dx, cx
	in al, dx
	mov cl, al
	ret

inw:	mov dx, cx
	in ax, dx
	mov cx, ax
	ret

ind:	mov dx, cx
	in eax, dx
	mov ecx, eax
	ret

outb:	mov al, dl
	mov dx, cx
	out dx, al
	ret

outw:	mov ax, dx
	mov dx, cx
	out dx, ax
	ret

outd:	mov eax, edx
	mov dx, cx
	out dx, eax
	ret
