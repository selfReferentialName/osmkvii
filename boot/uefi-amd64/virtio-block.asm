	;; virtio-block.asm
	;;
	;; handles block devices (virtual hard drives) using the virtio protocol

section .rodata

found_msg: db "Initializing a virtio block device - ", 0
ok_msg:	db "[OKAY]", 0x0a, 0x0d, 0
fail0_msg: db "[FAIL 0]", 0x0a, 0x0d, 0
fail1_msg: db "[FAIL 1]", 0x0a, 0x0d, 0
fail2_msg: db "[FAIL 2]", 0x0a, 0x0d, 0

section .bss
extern vda_port
extern vda_queue

section .text
global virtio_block_register
extern print
extern getpage

virtio_block_register:
	push rax
	mov rax, found_msg
	call print
	pop rax
	mov dx, 0xcf8
	add eax, 0x10
	out dx, eax
	mov dx, 0xcfc
	in eax, dx
	mov [rbx + vda_port], ax
	mov dx, ax			; load the port
	add dx, 0x12			; + 0x12
	mov al, 0x01
	out dx, al			; ack
	mov al, 0x03
	out dx, al			; driver loaded
	sub dx, 0x12			; + 0
	in eax, dx
	and rax, 0x20			; RO feature bit
	add dx, 0x04			; + 4
	out dx, eax			; negotiate
	add dx, 0x0d			; + 0x12
	mov al, 0x0b			; features OK
	out dx, al
	; negotiations complete, now to get queues
	xor di, di			; queue address
	sub dx, 0x6			; + 0xc
	mov ax, di
	add dx, 2			; + 0xe
	out dx, ax			; send queue address
	sub dx, 2			; + 0xc
	in ax, dx			; get the queue size
	cmp ax, 3000			; max supported size
	jg rfail0
	bsf cx, ax
	mov rax, 16 + 2 + 8
	shl rax, cl
	add rax, 4095 * 2
	shr rax, 12			; this is the number of pages we need to allocate
	push rcx
	push rdi
	push rax
	mov rcx, rax
rlp0:	push rcx			; this assumes that we are the first thing that can get pages
	call getpage
	pop rcx
	loop rlp0			; the most recent rax is the lowest
	mov r8, rax
	mov rdi, rax
	pop rcx
	shr rcx, 12 - 3
	xor rax, rax
	rep stosq			; zero out the pages
	add dx, 2			; + 0xe
	mov ax, [rsp]
	out dx, ax
	sub dx, 6			; + 8
	mov rax, r8
	out dx, eax
	mov [rbx + vda_queue], rax
	pop rdi
	pop rcx
	test r8, 0xffffffff80000000	; check r8 to see if it is above 32 bit address space
	jnz rfail2
	add dx, 0xa			; + 12
	mov al, 0x0f			; driver OK
	out dx, al
	in al, dx
	test al, 0x40
	jnz rfail1			; test the reset needed thing
	mov rax, ok_msg
	call print
	ret
rfail0:	mov rax, fail0_msg
	call print
	ret
rfail1:	mov rax, fail1_msg
	call print
	ret
rfail2:	mov rax, fail2_msg
	call print
	ret
