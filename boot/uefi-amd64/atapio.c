/* atapio.c
 *
 * handle the ata_read_c function
 */

#include "io.h"

#define ATA_DATA 0x1f0
#define ATA_ERROR ATA_DATA + 1
#define ATA_FEATURES ATA_DATA + 1
#define ATA_COUNT ATA_DATA + 2
#define ATA_LBA_LOW ATA_DATA + 3
#define ATA_LBA_MID ATA_DATA + 4
#define ATA_LBA_HIGH ATA_DATA + 5
#define ATA_DRV ATA_DATA + 6
#define ATA_STATUS ATA_DATA + 7
#define ATA_CMD ATA_DATA + 7
#define ATA_ALT_STATUS 0x3f6
#define ATA_CTRL ATA_ALT_STATUS
#define ATA_ADDR ATA_ALT_STATUS + 1

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

void ata_wait_bsy_rdy (void); // obvious, in the assembly
void ata_wait_bsy_drq (void);
void cprint (short*);

void ata_read_c (long long lba, short* buffer, long long count, unsigned short sectrsz) {
	for (; count > 0; count -= 256, lba += 256, buffer += sectrsz * 256) {
		ata_wait_bsy_rdy();
		// send the command
		outb(ATA_FEATURES, 0);
		outb(ATA_COUNT, 0xff & min(256, 0xff & count));
		outb(ATA_LBA_LOW, lba & 0xff);
		outb(ATA_LBA_MID, (lba >> 8) & 0xff);
		outb(ATA_LBA_HIGH, (lba >> 16) & 0xff);
		outb(ATA_DRV, 0xe0 | ((lba >> 24) & 0x0f));
		outb(ATA_CMD, 0x20);
		// read the sectors
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < sectrsz / 2; j++) {
				ata_wait_bsy_drq();
				buffer[i * sectrsz / 2 + j] = inw(ATA_DATA);
			}
		}
	}
}
