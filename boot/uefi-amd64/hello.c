/* hello.c
 *
 * manage the first steps of booting
 * from startup to the exit of boot services
 */

#include "efi.h"
#include "protocol/efi-sfsp.h"
#include "protocol/efi-fp.h"

void mothership (EFI_PHYSICAL_ADDRESS info);

struct file {
	EFI_PHYSICAL_ADDRESS buffer;
	long long size;
};

EFI_GUID simple_fs_guid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;

EFI_HANDLE handle;
EFI_SYSTEM_TABLE* sys_table;
EFI_PHYSICAL_ADDRESS free_page_ptr = 0;
EFI_TIME time;
long long timens;
volatile int config[16];
EFI_PHYSICAL_ADDRESS info;
struct file* files;
EFI_FILE_PROTOCOL* rootdir;

void* get_page(void) {
	void* result = free_page_ptr;
	free_page_ptr = *((void**) free_page_ptr);
	return result;
}

CHAR16* message = (CHAR16*)L"Hello World!\n\r";

EFI_STATUS cprint (CHAR16* in) // just a wrapper for the overly long print command
{
	return sys_table->ConOut->OutputString(sys_table->ConOut, in);
}

void cutime (void)
{
	sys_table->RuntimeServices->GetTime(&time, 0);
	timens = (unsigned long long) time.Nanosecond + 1000000000ULL * 
		((unsigned long long) time.Second + 60ULL *
		 ((unsigned long long) time.Minute + 60ULL *
		  ((unsigned long long) time.Hour + 24ULL *
		   ((unsigned long long) time.Day))));
}

inline CHAR16 dtoc (int d) // digit to character
{
	return L'0' + (short) d;
}

// for the ltoa family, make sure a has more that log10(max_l)+1 bytes in it

void ltoa (long long l, CHAR16* a) // long long int to string
{
	int i = 0;
	int s = 0;
	if (l < 0) {
		a[0] = L'-';
		i++;
		l = -l;
		s = 1;
	}
	for (; l != 0; i++) {
		a[i] = dtoc(l % 10);
		l /= 10;
	}
	for (int j = s; j < i - j - 1 + s; j++) {
		CHAR16 tmp = a[j];
		a[j] = a[i - j - 1 + s];
		a[i - j - 1] = tmp;
	}
	a[i] = 0;
}

void ultoa (unsigned long long l, CHAR16* a) // long int to string
{
	int i = 0;
	for (; l != 0; i++) {
		a[i] = dtoc(l % 10);
		l /= 10;
	}
	for (int j = 0; j < i - j - 1; j++) {
		CHAR16 tmp = a[j];
		a[j] = a[i - j - 1];
		a[i - j - 1] = tmp;
	}
	a[i] = 0;
}

EFI_STATUS efi_main(EFI_HANDLE h, EFI_SYSTEM_TABLE* st)
{
	handle = h;
	sys_table = st;
	cprint(message);
	CHAR16 a[24];

	EFI_MEMORY_DESCRIPTOR* memmap;
	UINTN mm_size = 0;
	UINTN mm_key;
	UINTN mm_desc_size; UINT32 trash1;
	if (sys_table->BootServices->AllocatePages(AllocateAnyPages, EfiLoaderData, 1, &info) != EFI_SUCCESS) {
		cprint((CHAR16*)L"Unable to allocate pages for boot info.\n\r");
		while (1);
	}
	while (1) {
		EFI_PHYSICAL_ADDRESS page;
		mm_size = 0;
		if (sys_table->BootServices->AllocatePages(AllocateAnyPages, EfiLoaderData, mm_size / 4096 + 1, (EFI_PHYSICAL_ADDRESS*) &memmap) != EFI_SUCCESS) {
			cprint((CHAR16*)L"Cannot allocate pages for memory map, please put an issue on the OSMKVII gitlab page.");
			while (1);
		}
		// OVMF doesn't like adding just one page at a time, so we add 16 at a time for it
		if (sys_table->BootServices->AllocatePages(AllocateAnyPages, EfiLoaderData, 16, &page) != EFI_SUCCESS) {
			cprint((CHAR16*)L"Allocated all memory.\n\r");
			break;
		}
		for (int i = 0; i < 16; i++, page += 0x1000) {
			if (free_page_ptr != 0) {
				*((EFI_PHYSICAL_ADDRESS**) page) = free_page_ptr;
			}
			free_page_ptr = page;
		}
	}
	mm_size = mm_size + 4096;
	if (sys_table->BootServices->GetMemoryMap(&mm_size, memmap, &mm_key, &mm_desc_size, &trash1) != EFI_SUCCESS) {
		cprint((CHAR16*)L"Cannot get memory map, please put an issue on the OSMKVII gitlab page.");
		while (1);
	}
	cprint((CHAR16*)L"Ready to exit boot services.\n\r");
	sys_table->BootServices->ExitBootServices(handle, mm_key); // it is done
	cprint((CHAR16*)L"Boot services exited.\n\r");
	*((EFI_PHYSICAL_ADDRESS*) info) = free_page_ptr;
	cprint((CHAR16*)L"Ready to go to the mothership.\n\r");
	mothership(info);

	return EFI_SUCCESS;
}
