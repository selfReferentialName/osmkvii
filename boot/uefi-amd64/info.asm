	;; info.asm
	;;
	;; handles the offsets for information pages

section .bss
global vda_port
global vda_queue
global free_page
global gdt
global gdtr
global idt
global idtr
global pit_count
global apic_ps
global sleep_done
global tss
global da_type
global ata_id
global esp_lba
global sectrsz
global esp_rdr
global fat_rdr
global cstrsz
global fat_st0
global fat_lbo
global ctrstr
global esp_sz
global kernel

	struc ipg
free_page:
	resq 1
vda_queue:
	resq 1
vda_port:
	resw 1
pit_count:
	resw 1
sleep_done:
	resb 1
da_type:resb 1			; 0 is none, 1 is ATAPIO, 2 is AHCI, 3 is virtio
idtr:	resw 1
idt:	resq 1
apic_ps:resq 1
sectrsz:resw 1
esp_sz:	resb 1
	resb 1			; padding
ata_id:	resb 1
ctrstr:	resb 1
gdtr:	resw 1
gdt:	resq 1
	resq 4			; part of the GDT
tss:	resb 0x68
esp_lba:resq 1			; lba of the start of the EFI system partition
esp_rdr:resq 1			; root directory page of page pointers
fat_lbo:resd 1
cstrsz:	resd 1
fat_st0:resq 1
kernel:	resq 1
	endstruc
