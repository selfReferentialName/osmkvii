#ifndef BOOT_IO_H
#define BOOT_IO_H

unsigned char inb (unsigned short);
unsigned short inw (unsigned short);
unsigned int ind (unsigned short);
void outb (unsigned short, unsigned char);
void outw (unsigned short, unsigned short);
void outd (unsigned short, unsigned int);

#endif
