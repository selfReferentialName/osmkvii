	;; gdt.asm
	;;
	;; a temporary gdt for before we load up greyhound
	;; note that the gdt is loaded from immediates because the .rodata segment doesn't work that well
	;; rbx points to a struct with all the important global variables in it
	;; see info.asm to get the definition of that struct
	;; also, credit to the linux kernel for all the flags to set in the code and data segments

section .rodata
extern gdtr
extern gdt
extern idtr
extern idt
extern tss

ckpoint:db "rax, al. Don't talk to me or my son ever again.", 0x0a, 0x0d, 0

section .text
global gdt_init
global cs_swap
extern print
extern idt_write

gdt_init:
	mov ax, 0x0028
	mov [rbx + gdtr], ax		; set the gdt size (I know hardcoded is bad, but I can't figure out a way not to hardcode it)
	mov rax, rbx
	add rax, gdt
	mov [rbx + gdt], rax		; set the null segment aka the gdt pointer
	mov rax, 0x00af9800
	shl rax, 32
	mov [rbx + gdt + 0x8], rax	; code segment 0x8
	mov rax, 0x00cf9200
	shl rax, 32
	mov [rbx + gdt + 0x10], rax	; data segment 0x10
	xor rax, rax
	lea rdi, [rbx + tss]
	mov rcx, 0x68 / 8
	rep stosq			; clear out the TSS for sanity's sake
	lea rax, [rbx + tss]		; this points to the TSS
	mov rcx, 0x68			; this is the limit of the TSS -- just enough to fit the TSS
	mov [rbx + gdt + 0x18], cx
	mov [rbx + gdt + 0x1a], ax
	shr rax, 16
	mov [rbx + gdt + 0x1c], al
	shr rax, 8
	mov dl, 0x89
	mov [rbx + gdt + 0x1d], dl	; the type -- figured out from the AMD manual
	mov dl, 0x10
	mov [rbx + gdt + 0x1e], dl	; some sanity stuff
	mov [rbx + gdt + 0x1f], al
	shr rax, 8
	mov [rbx + gdt + 0x20], rax	; task state segment 0x18
	cli
	lgdt [rbx + gdtr]
	mov ax, 0x18
	ltr ax
	mov rax, ckpoint
	call print
	ret

cs_swap:mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	lea rax, [rel slb0]
	mov dx, 0x8eef
	call idt_write
	int 0xef
slb0:	sti				; actually remember to re-enable interrupts
	add rsp, 40
	ret
