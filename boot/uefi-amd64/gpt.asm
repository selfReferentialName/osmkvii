	;; gpt.asm
	;;
	;; handle the GUID partition table
	;; assumes that the sector size is at most 4096

section .rodata

esp_guid:
	dd 0xc12a7328
	dw 0xf81f
	dw 0x11d2
	db 0xba, 0x4b
	db 0x00, 0xa0, 0xc9, 0x3e, 0xc9, 0x3b
ierror:	db "Invalid GUID Partition Table.", 0x0a, 0x0d, 0
nerror:	db "Cannot find EFI system partition.", 0x0a, 0x0d, 0
ckpoint:db "Scoobady Doobady coming for that booty.", 0x0a, 0x0d, 0

section .bss
extern esp_lba

section .text
global gpt_init
extern print
extern da_read
extern getpage
extern freepage
extern printrax

gpt_init:
	call getpage
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	push rdx
	call da_read			; read the GPT headder
	pop rsi
	mov rax, [rsi]
	mov r8, 0x5452415020494645
	cmp rax, r8
	jne ifail			; check for a valid partition table
	push rsi
	mov rax, ckpoint
	call print
	pop rdx
	%assign i 0
	%rep 32
	push rdx
	mov rax, 2 + i
	mov rdi, 1
	call da_read
	pop rdx
	mov rsi, rdx
	lea rdi, [rel esp_guid]
	mov rcx, 2
	repe cmpsq
	je ilb0
	%assign i i + 1
	%endrep
	jmp nfail
ilb0:	mov rax, [rdx + 0x20]
	mov [rbx + esp_lba], rax
	ret

nfail:	mov rax, nerror
	call print
	jmp halt

ifail:	mov rax, ierror
	call print
halt:	hlt
	jmp halt
