	;; fat.asm
	;;
	;; handle the fat32 filesystem of the EFI system partition
	;; currently, it only supports loading from the root directory

section .bss
extern esp_lba
extern fat_lbo
extern esp_rdr
extern sectrsz
extern cstrsz
extern fat_st0				; LBO of cluster 2
extern ctrstr				; sectors per cluster
extern esp_sz				; variety of fat the ESP is

section .rodata

lerror:	db "Programmer was too lazy to implement a feature, please bug them to add it in.", 0x0a, 0x0d, 0
ferror:	db "File not found.", 0x0a, 0x0d, 0
ckpoint:db "/(.u.)\ Writing useful debug messages. \(^v^)/ Writing memes for debugging.", 0x0a, 0x0d, 0
done0_msg:
	db "Initialized FAT-", 0
done1_msg:
	db " ESP filesystem.", 0x0a, 0x0d, 0

section .text
global esp_init
global readfile				; rax:rdi is the 8.3 name (first character in first byte), rdx is the page to store an array of page pointers (max file size is 2MB). returns rax = size
extern da_read
extern getpage
extern freepage
extern print
extern getpage0
extern printrax
extern printal

readfile:
	mov r8, [rbx + esp_rdr]
	mov rcx, 512
rflp0:	mov rsi, [r8 + 8 * (rcx - 1)]
	push rcx
	xor rcx, rcx
	cmp rsi, rcx
	je rflp0c
	mov cx, [rbx + sectrsz]
	sub rcx, 32
rflp0lp0:
	cmp rax, [rsi + rcx]
	jne rflp0lp0c
	mov r10d, [rsi + rcx + 8]
	and r10d, 0x00ffffff
	cmp r10d, edi
	jne rflp0lp0c
	mov rdi, rdx
	xor rdx, rdx
	mov ax, [rsi + rcx + 20]
	shl eax, 16
	mov ax, [rsi + rcx + 26]
	push rsi
	push rcx
	call readclusters
	pop rcx
	pop rsi
	mov eax, [rsi + rcx + 28]
	pop rcx
	ret
rflp0lp0c:
	sub rcx, 32
	ja rflp0lp0
rflp0c:	pop rcx
	loop rflp0			; uncomment when debugging code is removed
	jmp ffail

readclusters:
	mov r10b, [rbx + esp_sz]
	cmp r10b, 12
	je readclusters12
	cmp r10b, 16
	je readclusters16
	jmp readclusters32

readclusters12:				; read the clusters at cluster rax starting at offset rdx of pointer page rdi
	push rax
	push rdx
	push rdi
	sub rax, 2
	xor rdx, rdx
	xor rcx, rcx
	mov cl, [rbx + ctrstr]
	mul ecx
	shl rdx, 32
	or rax, rdx			; get the LBO
	add rax, [rbx + fat_st0]
	add rax, [rbx + esp_lba]	; get the LBA
r2lp0:	cmp cl, 4
	jl lerror			; small cluster sizes are a TODO
	push rax
	push rcx
	call getpage
	push rax
	push rcx
	push rdx
	push rdi
	push rsi
	mov rax, [rsp + 0x30]
	call printrax
	pop rsi
	pop rdi
	pop rdx
	pop rcx
	pop rax
	mov rdx, rax
	mov r8, [rsp + 0x10]
	mov r9, [rsp + 0x18]
	mov [r8 + r9], rdx		; save the page we're putting stuff at
	mov rdi, 4
	call da_read			; read the first page of the cluster
	pop rcx
	pop rax
	add rax, 4
	sub rcx, 4
	ja r2lp0
	mov rax, [rsp + 0x10]
	shr rax, 10
	push rdx
	xor rdx, rdx
	mov rcx, 3
	mul rcx
	pop rdx
	add rax, [rbx + fat_lbo]
	add rax, [rbx + esp_lba]	; LBA of the sector containing the FAT entry of this cluster
	mov rdi, 3
	push rax
	push rdi
	mov rax, ckpoint
	call print
	call getpage0			; get the page to put the sector
	mov rdx, rax
	pop rdi
	pop rax
	push rdx
	call da_read			; read the sector
	pop rsi
	pop rdi
	pop rdx
	pop rax
	mov [rdi + rdx], rsi		; save the pointer to the sectors
	mov r9, rax
	and r9, 0x3fe			; extract the offset of the FAT entry in the sectors in two entries (3 bytes)
	push rax
	push rdx
	xor rdx, rdx
	mov rax, 3
	mul r9
	mov r9, rax			; convert it to bytes
	pop rdx
	pop rax
	mov r10b, al
	and r10b, 1			; get the offset within the byte in nibbles
	xor r8, r8
	mov r8w, [rsi + r9]		; get the entry ready
	cmp r10b, 1
	jne r2lb0
	shr r8w, 4
r2lb0:	and r8w, 0x0fff			; extract the entry
	cmp r8w, 0x0ff7
	jge r6done
	mov rax, r8
	add rdx, 8
	jmp readclusters12		; recursion FTW
r2done:	xor rax, rax
	add rdx, 8
r2dlp0:	mov [rdi + rax], rax
	add rdx, 8
	cmp rdx, 4096
	jl r2dlp0
	ret

readclusters16:				; read the clusters at cluster rax starting at offset rdx of pointer page rdi
	push rax
	push rdx
	push rdi
	xor rdx, rdx
	xor rcx, rcx
	mov cl, [rbx + ctrstr]
	mul ecx
	shl rdx, 32
	or rax, rdx			; get the LBA
r6lp0:	cmp cl, 4
	jl lerror			; small cluster sizes are a TODO
	push rax
	push rcx
	call getpage
	mov rdx, rax
	mov r8, [rsp + 0x10]
	mov r9, [rsp + 0x18]
	mov [r8 + r9], rdx		; save the page we're putting stuff at
	mov rdi, 4
	call da_read			; read the first page of the cluster
	pop rcx
	pop rax
	add rax, 4
	sub rcx, 4
	jnz r6lp0
	mov rax, [rsp + 0x10]
	shr rax, 8
	add rax, [rbx + fat_lbo]
	add rax, [rbx + esp_lba]	; LBA of the sector containing the FAT entry of this cluster
	mov rdi, 1
	push rax
	push rdi
	call getpage0			; get the page to put the sector
	mov rdx, rax
	pop rdi
	pop rax
	push rdx
	call da_read			; read the sector
	pop rsi
	pop rdi
	pop rdx
	pop rax
	mov [rdi + rdx], rsi		; save the pointer to the sectors
	mov r9, rax
	and r9, 0xff			; extract the offset of the FAT entry in the sector
	shl r9, 1
	xor r8, r8
	mov r8w, [rsi + r9]		; get the entry
	cmp r8w, 0xfff7
	jge r6done
	mov rax, r8
	add rdx, 8
	jmp readclusters16		; recursion FTW
r6done:	xor rax, rax
	add rdx, 8
r6dlp0:	mov [rdi + rax], rax
	add rdx, 8
	cmp rdx, 4096
	jl r6dlp0
	ret

readclusters32:				; read the clusters at cluster rax starting at offset rdx of pointer page rdi
	push rax
	push rdx
	push rdi
	xor rdx, rdx
	xor rcx, rcx
	mov cl, [rbx + ctrstr]
	mul ecx
	shl rdx, 32
	or rax, rdx			; get the LBA
rclp0:	cmp cl, 4
	jl lerror			; small cluster sizes are a TODO
	push rax
	push rcx
	call getpage
	mov rdx, rax
	mov r8, [rsp + 0x10]
	mov r9, [rsp + 0x18]
	mov [r8 + r9], rdx		; save the page we're putting stuff at
	mov rdi, 4
	call da_read			; read the first page of the cluster
	pop rcx
	pop rax
	add rax, 4
	sub rcx, 4
	jnz rclp0
	mov rax, [rsp + 0x10]
	shr rax, 7
	add rax, [rbx + fat_lbo]
	add rax, [rbx + esp_lba]	; LBA of the sector containing the FAT entry of this cluster
	mov rdi, 1
	push rax
	push rdi
	call getpage0			; get the page to put the sector
	mov rdx, rax
	pop rdi
	pop rax
	push rdx
	call da_read			; read the sector
	pop rsi
	pop rdi
	pop rdx
	pop rax
	mov [rdi + rdx], rsi		; save the pointer to the sectors
	mov r9, rax
	and r9, 0x7f			; extract the offset of the FAT entry in the sector
	shl r9, 2
	mov r8d, [rsi + r9]		; get the entry
	cmp r8d, 0xfffffff7
	jge rcdone
	mov rax, r8
	add rdx, 8
	jmp readclusters32		; recursion FTW
rcdone:	xor rax, rax
	add rdx, 8
rcdlp0:	mov [rdi + rax], rax
	add rdx, 8
	cmp rdx, 4096
	jl rcdlp0
	ret

esp_init:
	call getpage			; get a page for the bios parameter block
	mov rsi, rax
	push rsi
	mov rdx, rax
	mov rdi, 1
	mov rax, [rbx + esp_lba]
	call da_read			; read the BIOS Parameter Block
	pop rsi
	mov al, 12
	mov cl, 32
	mov dx, [rsi + 0x13]
	test dx, dx			; if zero, the ZF flag will be set, thus making je jump
	cmove ax, cx
	je ilb0
	mov cl, 16
	cmp dx, 4085
	cmovge ax, cx
ilb0:	mov [rbx + esp_sz], al
	mov ax, [rsi + 0xb]
	cmp ax, [rbx + sectrsz]
	jne lfail
	xor cx, cx
	mov cl, [rsi + 0xd]
	mov [rbx + ctrstr], cl		; get the cluster size in sectors
	xor dx, dx
	mul cx
	shl edx, 16
	mov dx, ax
	mov [rbx + cstrsz], edx		; get the cluster size in bytes
	xor rax, rax
	mov ax, [rsi + 0xe]		; sector offset of the FAT
	mov [rbx + fat_lbo], rax
	xor rcx, rcx
	mov cl, [rsi + 0x10]		; number of fats
	mov rdi, rax
	mov r8d, [rsi + 0x24]
	xor rax, rax
	mov ax, [rsi + 0x16]
	mov r10b, [rbx + esp_sz]
	cmp r10b, 32
	cmove rax, r8			; size of the FAT
	xor rdx, rdx
	mul ecx
	shl rdx, 32
	or rax, rdx
	add rax, rdi			; first sector of root directory (FAT12/FAT16) or first data sector (FAT32)
	cmp r10b, 32
	je irt32
	mov rdi, rax
	xor rdx, rdx
	xor rax, rax
	mov ax, [rsi + 0x11]
	shl eax, 5
	xor r8, r8
	mov r8w, [rbx + sectrsz]
	div r8				; size of the root directory in sectors
	mov rcx, rax
	add rax, rdi
	mov [rbx + fat_st0], rax	; first data sector
	mov [rbx + esp_rdr], rsi	; save where we're gonna put the root directory
	add rdi, [rbx + esp_lba]	; take the position of the esp while reading the root directory sectors
	xor rax, rax
ilp0:	mov qword [rsi + rax], 0
	add rax, 8
	cmp rax, 4096
	jl ilp0				; zero the page
ilp1:	push rsi			; page of pointers plus the offset where we are in it
	push rcx			; size of the root directory in sectors remaining
	push rdi			; current sector we're working on
	call getpage0			; get a page to put the sectors in
	mov rdx, rax
	mov rdi, [rsp + 0x8]
	cmp rdi, 4
	jl ilb1
	mov rdi, 4
	jmp ilb2
ilb1:	and rdi, 3
ilb2:	mov rax, [rsp]
	push rdx
	call da_read			; read the sectors
	pop r8
	pop rdi
	pop rcx
	pop rsi
	mov [rsi], r8
	add rsi, 8
	add rdi, 4
	sub rcx, 4
	jna ilp1			; na tests for CF = 1 or ZF = 1. if we read the last 3 or less, CF is set, if we read the last 4 sectors, ZF is set
	jmp idone
irt32:	mov [rbx + fat_st0], rax	; first data sector
	mov [rbx + esp_rdr], rsi	; save where we're gonna put the root directory
	mov rdi, rsi
	xor rdx, rdx
	mov eax, [rsi + 0x2c]		; cluster of the root directory
	call readclusters32
idone:	mov rax, done0_msg
	call print
	mov al, [rbx + esp_sz]
	call printal
	mov rax, done1_msg
	jmp print

lfail:	mov rax, lerror
	call print
halt:	hlt
	jmp halt

ffail:	mov rax, ferror
	call print
	jmp halt
