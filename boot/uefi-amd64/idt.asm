	;; idt.asm
	;;
	;; manages the interrupt descriptor table
	;; also handles interrupts

section .bss
extern idtr
extern idt

	struc idte			; idt entry
off0_15:resw 1				; offset bits 0 to 15
segment:resw 1
ist:	resb 1				; interrupt stack table
type:	resb 1				; type and attributes
off16_31:
	resw 1
off32_64:
	resd 1
	resd 1				; reserved
	endstruc

section .rodata

de_msg:	db "Tried to divide by zero. Somehow.", 0x0a, 0x0d, 0
db_msg:	db "Who tried to turn on debug mode? It caused an exception.", 0x0a, 0x0d, 0
nmi_msg:db "NMI fired. Cannot boot. Please turn the computer off and back on again, and maybe try a different OS.", 0x0a, 0x0d, 0
bp_msg:	db "Breakpoint passed. Probably jumped to the wrong place or forgot a ret.", 0x0a, 0x0d, 0
of_msg:	db "Overflow exception. This was for some reason intentional. No idea why.", 0x0a, 0x0d, 0
br_msg:	db "Something was out of bounds.", 0x0a, 0x0d, 0
ud_msg:	db "Wait, that's illegal. Tried to run nonvalid code.", 0x0a, 0x0d, 0
nm_msg:	db "Coprocessor not available. This should be impossible. FPUs are integrated on all amd64 chips.", 0x0a, 0x0d, 0
df_msg:	db "Some kind of problem with an exception handler.", 0x0a, 0x0d, 0
ts_msg:	db "TSS invalid. Shouldn't be using a TSS yet anyways.", 0x0a, 0x0d, 0
np_msg:	db "Invalid segment. Some kind of problem with the GDT likely.", 0x0a, 0x0d, 0
ss_msg:	db "Stack is in the wrong place", 0x0a, 0x0d, 0
gp_msg:	db "General protection fault. No idea why.", 0x0a, 0x0d, 0
pf_msg:	db "Page fault. Are you sure you didn't try to load something outside of dynamic memory?", 0x0a, 0x0d, 0
mf_msg:	db "Some kind of error with the FPU.", 0x0a, 0x0d, 0
ac_msg: db "Alignment check failed. Something is very wrong.", 0x0a, 0x0d, 0
mc_msg:	db "MC fired. Cannot boot. Please turn the computer off and back on again, and maybe try a different OS.", 0x0a, 0x0d, 0
xm_msg:	db "SMID error.", 0x0a, 0x0d, 0
ve_msg:	db "Virtualization exception. Something is extremely wrong.", 0x0a, 0x0d, 0
wr0_msg:db "Written IDT entry 0x", 0
wr1_msg:db ".", 0x0a, 0x0d, 0
rax_msg:db 0x0a, 0x0d, "rax: ", 0
rbx_msg:db 0x0a, 0x0d, "rbx: ", 0
rcx_msg:db 0x0a, 0x0d, "rcx: ", 0
rdx_msg:db 0x0a, 0x0d, "rdx: ", 0
rdi_msg:db 0x0a, 0x0d, "rdi: ", 0
rsi_msg:db 0x0a, 0x0d, "rsi: ", 0
rsp_msg:db 0x0a, 0x0d, "rsp: ", 0
rbp_msg:db 0x0a, 0x0d, "rbp: ", 0
r8_msg:	db 0x0a, 0x0d, "r8: ", 0
r9_msg:	db 0x0a, 0x0d, "r9: ", 0
r10_msg:db 0x0a, 0x0d, "r10: ", 0
r11_msg:db 0x0a, 0x0d, "r11: ", 0
r12_msg:db 0x0a, 0x0d, "r12: ", 0
r13_msg:db 0x0a, 0x0d, "r13: ", 0
r14_msg:db 0x0a, 0x0d, "r14: ", 0
r15_msg:db 0x0a, 0x0d, "r15: ", 0
rip_msg:db 0x0a, 0x0d, "rip: ", 0
rflags_msg:
	db 0x0a, 0x0d, "rflags: ", 0
cs_msg:	db 0x0a, 0x0d, "cs: ", 0
ssg_msg:db 0x0a, 0x0d, "ss: ", 0

section .text
global idt_init
global idt_write
extern getpage
extern print
extern printrax

idt_write:				; rax holds the "offset", and dl holds the interrupt number, and dh holds the type
	mov rdi, [rbx + idt]
	xor rcx, rcx
	mov cl, dl
	shl rcx, 4
	add rdi, rcx			; rdi holds the place to put it
	mov [rdi], ax			; offset 0-15
	mov cx, 0x08
	mov [rdi + 2], cx		; code segment
	xor cl, cl
	mov [rdi + 4], cl		; various stuff we need set to zero
	mov [rdi + 5], dh		; type
	shr rax, 16
	mov [rdi + 6], ax		; offset 16-32
	shr rax, 16
	mov [rdi + 8], rax		; offset 32-64 & reserved stuff
	ret

ac_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, ac_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

bp_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, bp_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

br_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, br_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

db_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, db_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

de_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, de_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

df_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, df_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

gp_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, gp_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

mc_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, mc_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

mf_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, mf_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

nm_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, nm_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

nmi_hand:
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, nmi_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

np_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, np_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

of_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, of_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

pf_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, pf_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

ss_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, ss_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

ts_hand:add rsp, 8
	push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, ts_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

ud_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, ud_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

ve_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, ve_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

xm_hand:push r11
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, xm_msg
	call print
	pop rax
	pop rcx
	pop rdx
	pop rdi
	pop rsi
	pop r8
	pop r9
	pop r10
	pop r11
	jmp intdump

intdump:call regdump
	mov rax, rip_msg
	call print
	pop rax
	call printrax
	mov rax, cs_msg
	call print
	pop rax
	call printrax
	pop rax
	mov rax, rflags_msg
	call print
	pop rax
	call printrax
	mov rax, rsp_msg
	call print
	pop rax
	call printrax
	mov rax, ssg_msg
	call print
	pop rax
	call printrax
halt:	hlt
	jmp halt

regdump:push r11			; dump all general purpose registers
	push r10
	push r9
	push r8
	push rsi
	push rdi
	push rdx
	push rcx
	push rax
	mov rax, rax_msg
	call print
	pop rax
	call printrax
	mov rax, rbx_msg
	call print
	mov rax, rbx
	call printrax
	mov rax, rcx_msg
	call print
	pop rax
	call printrax
	mov rax, rdx_msg
	call print
	pop rax
	call printrax
	mov rax, rdi_msg
	call print
	pop rax
	call printrax
	mov rax, rsi_msg
	call print
	pop rax
	call printrax
	mov rax, rbp_msg
	call print
	mov rax, rbp
	call printrax
	mov rax, r8_msg
	call print
	pop rax
	call printrax
	mov rax, r9_msg
	call print
	pop rax
	call printrax
	mov rax, r10_msg
	call print
	pop rax
	call printrax
	mov rax, r11_msg
	call print
	pop rax
	call printrax
	mov rax, r12_msg
	call print
	mov rax, r12
	call printrax
	mov rax, r13_msg
	call print
	mov rax, r13
	call printrax
	mov rax, r14_msg
	call print
	mov rax, r14
	call printrax
	mov rax, r15_msg
	call print
	mov rax, r15
	call printrax
	ret

idt_init:
	; first, get a page
	call getpage
	mov r8, rax
	mov rdi, rax
	xor rax, rax
	mov rcx, 512
	rep stosq
	mov [rbx + idt], r8
	mov ax, 4096			; convientiently equal to 256 entries
	mov [rbx + idtr], ax
	lidt [rbx + idtr]
	; now we have a page, time to fill it
	mov rax, de_hand
	mov dx, 0x8e00
	call idt_write
	mov rax, db_hand
	mov dx, 0x8e01
	call idt_write
	mov rax, nmi_hand
	mov dx, 0x8e02
	call idt_write
	mov rax, bp_hand
	mov dx, 0x8e03
	call idt_write
	mov rax, of_hand
	mov dx, 0x8e04
	call idt_write
	mov rax, br_hand
	mov dx, 0x8e05
	call idt_write
	mov rax, ud_hand
	mov dx, 0x8e06
	call idt_write
	mov rax, nm_hand
	mov dx, 0x8e07
	call idt_write
	mov rax, df_hand
	mov dx, 0x8e08
	call idt_write
	mov rax, ts_hand
	mov dx, 0x8e0a
	call idt_write
	mov rax, np_hand
	mov dx, 0x8e0b
	call idt_write
	mov rax, ss_hand
	mov dx, 0x8e0c
	call idt_write
	mov rax, gp_hand
	mov dx, 0x8e0d
	call idt_write
	mov rax, pf_hand
	mov dx, 0x8e0e
	call idt_write
	mov rax, mf_hand
	mov dx, 0x8e10
	call idt_write
	mov rax, ac_hand
	mov dx, 0x8e11
	call idt_write
	mov rax, mc_hand
	mov dx, 0x8e12
	call idt_write
	mov rax, xm_hand
	mov dx, 0x8e13
	call idt_write
	mov rax, ve_hand
	mov dx, 0x8e14
	call idt_write
	ret
