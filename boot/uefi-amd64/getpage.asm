	;; getpage.asm
	;;
	;; handles the allocation of pages
	;; also handles the freeing of pages

section .bss
extern free_page

section .text
global getpage
global freepage
global getpage0

getpage:mov rax, [rbx + free_page]
	mov rdx, [rax]
	mov [rbx + free_page], rdx
	ret

getpage0:
	call getpage
	mov rdi, rax
	mov rsi, rax
	xor rax, rax
	mov rcx, 512
	rep stosq
	mov rax, rsi
	ret

freepage:
	mov rdx, [rbx + free_page]
	mov [rax], rdx
	mov [rbx + free_page], rax
	ret
