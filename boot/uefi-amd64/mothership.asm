	;; mothership.asm
	;;
	;; call all the important functions after hello.c is done

section .bss
extern da_type

section .rodata

greyhound:
	db "GRYHOUND", "BIN", 0
init_msg:
	db "Mothership loaded. Loading GDT.", 0x0a, 0x0d, 0
idt_msg:db "Loading IDT.", 0x0a, 0x0d, 0
apic_msg:
	db "Initializing APIC.", 0x0a, 0x0d, 0
pci_init_msg:
	db "Going over PCI devices.", 0x0a, 0x0d, 0
done_msg:
	db "Nothing left to do, just resting now.", 0x0a, 0x0d, 0
ata_msg:db "No modern drive found, falling back to ATA.", 0x0a, 0x0d, 0
noda_msg:
	db "No hard drive found. Cannot boot", 0x0a, 0x0d, 0
gpt_msg:db "Finding EFI system partition.", 0x0a, 0x0d, 0
esp_msg:db "Loading root directory of EFI system partition.", 0x0a, 0x0d, 0
krn_msg:db "Reading kernel from device.", 0x0a, 0x0d, 0

section .bss
extern kernel

section .text
global mothership
global da_read
extern print
extern print_init
extern pci_init
extern gdt_init
extern idt_init
extern apic_init
extern cs_swap
extern ata_init
extern ata_read
extern gpt_init
extern esp_init
extern readfile
extern getpage
extern printrax
extern printax

mothership:
	mov rbx, rcx
	mov rax, init_msg
	call print
	call gdt_init
	mov rax, idt_msg
	call print
	call idt_init
	call cs_swap
	mov rax, apic_msg
	call print
	call apic_init
	xor al, al
	mov [rbx + da_type], al
	mov rax, pci_init_msg
	call print
	call pci_init
	xor al, al
	cmp [rbx + da_type], al
	jne mlb0
	mov rax, ata_msg
	call print
	call ata_init
mlb0:	mov rax, gpt_msg
	call print
	call gpt_init
	mov rax, esp_msg
	call print
	call esp_init
	mov rax, krn_msg
	call print
	call getpage
	mov rdx, rax
	mov [rbx + kernel], rax
	mov rax, [rel greyhound]
	mov edi, [rel greyhound + 8]
	and edi, 0x00ffffff
	call readfile
	mov rax, done_msg
	call print
halt:	hlt
	jmp halt

da_read:mov r11b, [rbx + da_type]
	cmp r11b, 1
	je ata_read
	mov rax, noda_msg
	call print
	jmp halt
