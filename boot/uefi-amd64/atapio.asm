	;; atapio.asm
	;;
	;; a driver for ATA port i/o mode
	;; 
	;; layout of ata_id
	;; bit 0: supports 32-bit reading and writing
	;; bit 1: supports DMA
	;; bit 2: supports 48-bit addressing

section .rodata

found_hdd:
	db "Found valid ATAPIO drive with a valid GPT table.", 0x0a, 0x0d, 0
derror0:db "Drive had error. Code is: 0x", 0
derror1:db ", and status is 0x", 0
derror2:db ". Please put an issue on GitLab.", 0x0a, 0x0d, 0
ierror:	db "If you see this something is very wrong.", 0x0a, 0x0d, 0
merror:	db "Drive is missing or configured weirdly. If this is in error, put an issue on Gitlab.", 0x0a, 0x0d, 0
perror:	db "Drive is not an ATA drive, possibly it is a ATAPI drive.", 0x0a, 0x0d, 0
ckpoint:db "I just sawed this", 0x20, "debug message in half.", 0x0a, 0x0d, 0
terror:	db "ATA drive timed out.", 0x0a, 0x0d, 0
werror:	db "The drive is doing something really wierd. I don't trust it.", 0x0a, 0x0d, 0
oerror:	db "The drive is some piece of ancient technology older than me.", 0x0a, 0x0d, 0
gpt_magic:
	dq 0x5452415020494645

DATAPRT equ 0x1f0
CTRLPRT equ 0x3f6

section .bss
extern sleep_done
extern da_type
extern ata_id
extern sectrsz

section .text
global ata_init
global ata_read
global ata_wait_bsy_drq
global ata_wait_bsy_rdy
extern print
extern sleep
extern timeout
extern getpage
extern freepage
extern printrax
extern printal
extern ata_read_c

sfprintal:
	push rax
	push rcx
	push rdx
	push rdi
	push rsi
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
	call printal
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rsi
	pop rdi
	pop rdx
	pop rcx
	pop rax
	ret

ata_wait_bsy_drq:
dlp0:	mov dx, DATAPRT + 7
	in al, dx
	test al, 0x80
	jnz dlp0c
	test al, 0x08
	jnz dlp0b
	test al, 0x21
	jnz dfail
dlp0c:	mov rax, 1000000
	push rcx
	call sleep
	pop rcx
	loop dlp0			; wait for BSY to clear and RDY to set
	jmp tfail
dlp0b:	ret

ata_wait_bsy_rdy:
wlp0:	mov dx, DATAPRT + 7
	in al, dx
	test al, 0x80
	jnz wlp0c
	test al, 0x40
	jnz wlp0b
wlp0c:	mov rax, 1000000
	push rcx
	call sleep
	pop rcx
	loop wlp0			; wait for BSY to clear and RDY to set
	jmp tfail
wlp0b:	ret

ata_read:				; rax is the LBA, rdi is the count of 512-byte sectors (max 256), rdx is the destination
	mov rcx, rax
	mov rdx, rdx
	mov r8, rdi
	mov r9w, [rbx + sectrsz]
	sub rsp, 32
	call ata_read_c
	add rsp, 32
	ret

ata_init:
	mov al, 0x06
	mov dx, CTRLPRT
	out dx, al			; do a software reset
	mov rax, 5000000
	call sleep			; 5us delay
	mov al, 0x02
	mov dx, CTRLPRT
	out dx, al			; unset software reset
	mov rax, 2000000000
	call sleep			; 2ms delay
	mov rax, 30000000000000
	call timeout
	mov dx, DATAPRT + 7
ilp0:	in al, dx
	test al, 0x80
	jz ilp0b
	xor al, al
	cmp [rbx + sleep_done], al
	je ilp0
	jmp tfail			; timeout on the busy bit
ilp0b:	mov dx, DATAPRT + 1
	in al, dx			; sanity
	mov dx, DATAPRT + 2
	in al, dx
	cmp al, 1
;	jne mfail			; count should be 1
	mov dx, DATAPRT + 3
	in al, dx
	cmp al, 1
;	jne mfail			; number should be 1
	mov dx, DATAPRT + 6
	mov al, 0xe0
	out dx, al			; select the drive
	mov rax, 400000
	call sleep			; 400ns delay
	mov rax, ckpoint
	call print
	mov dx, DATAPRT + 7
	in al, dx
	test al, 0x01
	jnz dfail			; check for an error
	xor al, al
	mov [rbx + ata_id], al		; clear the identity bits
	call getpage
	mov rdi, rax			; get a page to put the identify stuff in
	mov rsi, rax
	xor al, al
	mov dx, DATAPRT + 1
	out dx, al
	mov dx, DATAPRT + 2
	out dx, al
	mov dx, DATAPRT + 3
	out dx, al
	mov dx, DATAPRT + 4
	out dx, al
	mov dx, DATAPRT + 5
	out dx, al
	mov al, 0xa0
	mov dx, DATAPRT + 6
	out dx, al			; register sanity
	mov al, 0xec
	mov dx, DATAPRT + 7
	out dx, al			; send the identify command
	push rdi
	push rsi
	mov rax, 400000
	call sleep
	mov rax, 1000000000000
	call timeout
	mov dx, DATAPRT + 7
ilp1:	xor al, al
	cmp [rbx + sleep_done], al
	jne tfail
	in al, dx
	test al, 0x80
	jnz ilp1			; wait for RDY to clear
ilp2:	xor al, al
	cmp [rbx + sleep_done], al
	jne tfail
	in al, dx
	test al, 0x08
	jz ilp2				; wait for DRQ to set
	in al, dx
	test al, 0x01
	jnz pfail			; ATAPI drives will error on identify (usually)
	test al, 0x20
	jnz dfail
	pop rsi
	pop rdi
	mov rcx, 256
	mov dx, DATAPRT
	rep insw			; get data from identify
	mov ax, [rsi]
	test ax, 0x8000
	jnz pfail			; double check that this isn't ATAPI
	mov ax, [rsi + 49 * 2]
	test ax, 0x200
	je ofail			; LBA mode is the only supported mode
	test ax, 0x100			; DMA supported
	jz ilb0
	mov al, 0x02
	or [rbx + ata_id], al
ilb0:	mov ax, [rsi + 83 * 2]
	test ax, 0x400			; 48-bit LBA
	jz ilb1
	mov al, 0x04
	or [rbx + ata_id], al
ilb1:	mov rax, rsi
	call freepage			; done with the identify block
	; TODO: check for 32-bit pio
	mov ax, 512
	mov [rbx + sectrsz], ax		; TODO: check sector size
	mov al, 1
	mov [rbx + da_type], al		; set the drive type
	ret

tfail:	mov rax, terror
	jmp print

ifail:	mov rax, ierror
	jmp print

halt:	hlt
	jmp halt

mfail:	mov rax, merror
	jmp print

pfail:	mov rax, perror
	jmp print

dfail:	mov rax, derror0
	call print
	mov dx, DATAPRT + 1
	in al, dx
	call printal
	mov rax, derror1
	call print
	mov dx, DATAPRT + 7
	in al, dx
	call printal
	mov rax, derror2
	call print
	jmp halt

wfail:	mov rax, werror
	jmp print

ofail:	mov rax, oerror
	jmp print
