	;; ahci.asm
	;;
	;; drive the advanced host controller interface

section .rodata

found_msg: db "Found an AHCI controller.", 0x0d, 0x0a, 0

section .text
global ahci_register
extern print

ahci_register:
	mov rax, found_msg
	call print
	ret
