	;; print.asm
	;;
	;; handle the print command after the hello environment

section .text
global print
global printax
global printrax
global print_init
global printal

print_init:
	mov dx, 0x3f9
	xor al, al
	out dx, al			; disable interrupts
	add dx, 2
	mov al, 0x80
	out dx, al			; set DLAB
	sub dx, 3
	mov al, 0x03
	out dx, al
	inc dx
	xor al, al
	out dx, al			; set baud rate to 38400
	add dx, 2
	mov al, 0x03
	out dx, al			; unset DLAB, set mode to 8N1
	dec dx
	mov al, 0x07
	out dx, al			; set up FIFO & clear it
	ret

print:	mov rdi, rax
plb0:	cmp byte [rdi], 0
	je return			; null terminated string
	mov dx, 0x3fd
plp0:	in al, dx
	test al, 0x20
	jz plp0				; test to make sure we can write stuff
	sub dx, 5
	mov al, [rdi]
	out dx, al			; output the byte
	inc rdi
	jmp plb0
return:	ret

printal:mov dil, al
	rol dil, 4
	mov rcx, 2
pblp0:	mov dx, 0x3fd
pblp1:	in al, dx
	test al, 0x20
	jz pblp1			; test to make sure we can write stuff
	sub dx, 5
	mov al, dil
	and al, 0x0f
	cmp al, 10
	jge pblb0
	add al, '0'
	jmp pblb1
pblb0:	sub al, 10
	add al, 'A'
pblb1:	out dx, al
	rol dil, 4
	loop pblp0
	ret

printax:mov di, ax
	rol di, 4
	mov rcx, 4
pwlp0:	mov dx, 0x3fd
pwlp1:	in al, dx
	test al, 0x20
	jz pwlp1			; test to make sure we can write stuff
	sub dx, 5
	mov al, dil
	and al, 0x0f
	cmp al, 10
	jge pwlb0
	add al, '0'
	jmp pwlb1
pwlb0:	sub al, 10
	add al, 'A'
pwlb1:	out dx, al
	rol di, 4
	loop pwlp0
	ret

printrax:
	mov rdi, rax
	rol rdi, 4
	mov rcx, 16
pqlp0:	mov dx, 0x3fd
pqlp1:	in al, dx
	test al, 0x20
	jz pqlp1			; test to make sure we can write stuff
	sub dx, 5
	mov al, dil
	and al, 0x0f
	cmp al, 10
	jge pqlb0
	add al, '0'
	jmp pqlb1
pqlb0:	sub al, 10
	add al, 'A'
pqlb1:	out dx, al
	rol rdi, 4
	loop pqlp0
	ret
